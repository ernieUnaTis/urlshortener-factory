import React, {Component } from 'react';
import axios from 'axios';
import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import { getToken,setUrlId } from './Utils/Common';
import './index.css';
class UrlShorts extends Component {


  constructor(props) {
    super(props)
    this.state = { urls: [] }
    this.deleteRow = this.deleteRow.bind(this);
  }





  componentDidMount() {



    const email = getToken();
    axios.post('http://192.168.1.164:8000/obtener_short_urls', {email:email })
    .then(response => {

      let urls = response.data.map(function(row,i) {
         return (
              <tr key={i}>
              <td><a href={"http://192.168.1.164:8000/shortUrl/" +row.url_short  }>http://192.168.1.164:8000/shortUrl/{row.url_short}</a></td>
              <td>{row.created_at}</td>
              <td>
               <a class="btn btn-primary" href="/verShortUrl" onClick={() => setUrlId(row.id)} role="button">Ver</a><span> </span>
               <a class="btn btn-success" href="#" onClick={() => this.deleteRow.bind(this)}  role="button">Eliminar</a>

              </td>
              </tr>
            )

          })

       this.setState({urls:urls})

     }).catch(error => {

     });
  }

  deleteRow = (e) => {
    alert("Hola");
  }

  render() {
    return (
      <div style={{ maxWidth: "100%" }}>



      <div className="container">
      <div className="row">
        <div className="col s12 board">
        <center><a class="btn btn-primary" href="/crearShortUrl" role="button">Agregar</a></center>
        <br/>
          <table class="table table-striped">
          <thead>
          <tr>
          <td>Short Url</td>
          <td>Creada</td>
          <td>Actions</td>
          </tr>
          </thead>
             <tbody>
             {this.state.urls}
             </tbody>
           </table>
        </div>
      </div>
    </div>
      </div>
    );


  }




}



export default UrlShorts;
