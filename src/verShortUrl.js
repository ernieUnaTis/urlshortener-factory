import React, { useState,useEffect,Component } from 'react';
import axios from 'axios';
import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import { getToken, getUrlId ,getApiKey } from './Utils/Common';


class VerShortUrl extends Component {
    constructor(props) {
      super(props)
      this.state = {
        url: [],
        stats: []
      }
    }

  componentWillMount() {
    const email = getToken();
    const urlId = getUrlId();
    const apiKey = getApiKey();
    axios.post('http://127.0.0.1:3000/obtener_info_short_url', {id:urlId,email: email }, {headers: {'apikey': apiKey} })
    .then(response => {
       this.setState({
        url:response.data
       })

       let stats = response.data.dispositivos.map(function(sD,i) {

         return (
              <tr key={i}>
                <td>{sD.dispositivo}</td>
                <td>{sD.user_agent}</td>
                <td>{sD.total}</td>
                <td>{sD.porcentaje}</td>
              </tr>
            )

          })
          this.setState(
            {
              stats:stats
            }
          )

     }).catch(error => {

     });
  }


  render() {
    return (
      <div>
      <h1>URL {this.state.url.url} </h1>
      <h1>Short url {this.state.url.url_short} </h1>
      <h1>Creado {this.state.url.created_at} </h1>
      <br/>
      <h1>Clicks {this.state.url.clicks} </h1>
      <div>
      <table class="table table-striped">
      <thead>
      <tr>
      <td>Dispositivo</td>
      <td>UserAgent</td>
      <td>Total</td>
      <td>Porcentaje</td>
      </tr>
      </thead>
         <tbody>
         {this.state.stats}
         </tbody>
       </table>
      </div>
      </div>
    );
  }
}
export default VerShortUrl;
