import axios from 'axios';

// return the user data from the session storage
export const getApiKey = () => {
  const apiKeyStr = sessionStorage.getItem('apikey');;
  if (apiKeyStr) return apiKeyStr;
  else return null;
}

// return the user data from the session storage
export const getUser = () => {
  const userStr = sessionStorage.getItem('nombre');
  if (userStr) return JSON.parse(userStr);
  else return null;
}

// return the token from the session storage
export const getToken = () => {
  return sessionStorage.getItem('email') || null;
}

// remove the token and user from the session storage
export const removeUserSession = () => {
  sessionStorage.removeItem('nombre');
  sessionStorage.removeItem('email');
}

// set the token and user from the session storage
export const setUserSession = (nombre, email,apikey) => {
  sessionStorage.setItem('nombre', nombre);
  sessionStorage.setItem('email', email);
  sessionStorage.setItem('apikey', apikey);
}

export const setRowsUrls = (rows) => {
  sessionStorage.setItem('rows', rows);
}

export const getRowsUrls = () => {
  return  sessionStorage.getItem('rows') || null;
}

export const setUrlId = (urlId) => {
  sessionStorage.setItem('urlId', urlId);
}

export const getUrlId = () => {
  return  sessionStorage.getItem('urlId') || null;
}
