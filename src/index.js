import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Route , NavLink , Switch} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';


import Home from './home';
import Login from './login';
import Registro from './registro';
import UrlShorts from './UrlShorts';
import CrearShortUrl from './crearShortUrl';
import VerShortUrl from './verShortUrl';
import Dashboard from './dashboard';

ReactDOM.render(

  <Router>
  <div>
    <Route path="/" component={Home}></Route>
    <Route path="/login" component={Login}></Route>
    <Route path="/registro" component={Registro}></Route>
    <Route path="/dashboard" component={Dashboard}></Route>
    <Route path="/UrlShorts" component={UrlShorts}></Route>
    <Route path="/CrearShortUrl" component={CrearShortUrl}></Route>
    <Route path="/VerShortUrl" component={VerShortUrl}></Route>
    </div>
  </Router>,

  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
