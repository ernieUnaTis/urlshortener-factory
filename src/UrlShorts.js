import React, {useState,useEffect,Component } from 'react';
import axios from 'axios';
import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import { getToken,setUrlId,setRemoveUrl,getApiKey } from './Utils/Common';
import './index.css';

class UrlShorts extends Component {


  constructor(props) {
    super(props);
    this.state = {
                  urls: [],
                  link_paginacion : ""
                };
  }





  componentDidMount() {
    const email = getToken();
    const apiKey = getApiKey();

     const setRemoveUrl=(id)=>{
      axios.post('http://127.0.0.1:3000/remove_short_url', {short_url:id ,email:email} , {headers: {'apikey': apiKey} })
      .then(response => {
           window.location.reload(false);
      }).catch(error => {


      });
    }


    axios.post('http://127.0.0.1:3000/obtener_short_urls', {email:email } , {headers: {'apikey': apiKey} })
    .then(response => {

      let link_paginacion = "http://127.0.0.1:3000/obtener_short_urls?page=2>; rel='next', <http://127.0.0.1:3000/obtener_short_urls?page=2>; rel='last'";
      let urls = response.data.map(function(row,i) {
         return (
              <tr key={i}>
              <td><a href={"http://127.0.0.1:3000/shortUrl/" +row.url_short  }>http://127.0.0.1:3000/shortUrl/{row.url_short}</a></td>
              <td>{row.created_at}</td>
              <td>
               <a class="btn btn-primary" href="/verShortUrl" onClick={() => setUrlId(row.id)} role="button">Ver</a><span> </span>
               <a class="btn btn-success" href="#" onClick={()=> setRemoveUrl(row.id)}  role="button">Eliminar</a>

              </td>
              </tr>
            )

          })

       this.setState({urls:urls})

       this.setState({link_paginacion:link_paginacion})

     }).catch(error => {

     });
  }





  render() {
    return (
      <div style={{ maxWidth: "100%" }}>
      <div className="container">
      <div className="row">
        <div className="col s12 board">
        <center><a class="btn btn-primary" href="/crearShortUrl" role="button">Agregar</a></center>
        <br/>
          <table class="table table-striped">
          <thead>
          <tr>
          <td>Short Url</td>
          <td>Creada</td>
          <td>Actions</td>
          </tr>
          </thead>
             <tbody>
             {this.state.urls}
             </tbody>
           </table>
            {this.state.link_paginacion}
        </div>
      </div>
    </div>
      </div>
    );


  }




}



export default UrlShorts;
