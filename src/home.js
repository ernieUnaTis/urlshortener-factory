import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route , NavLink , Switch} from 'react-router-dom'

import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import { getToken, removeUserSession, setUserSession } from './Utils/Common';



//export default class Home extends Component {
function Home(props) {

  const [authLoading, setAuthLoading] = useState(true);
  const email = getToken();

  const handleLogout = () => {
    removeUserSession();
    props.history.push('/login');
  }

  if (!email) {
    return (
      <div className="header">
        <NavLink activeClassName="active" to="/login">Login</NavLink>
        <NavLink activeClassName="active" to="/registro">Registro</NavLink>

      </div>

    );
  }else{
    return (
      <div className="header">
        <NavLink exact activeClassName="active" to="/UrlShorts">My Links</NavLink>
  

         <a class="btn btn-info" href="#" onClick={handleLogout}  role="button">Logout</a>{email}

      </div>
    );
  }

  //render() {

  //}
}

export default Home;
