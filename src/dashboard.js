import React, {Component } from 'react';
import axios from 'axios';
import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import { getToken,setUrlId,setRemoveUrl,getApiKey } from './Utils/Common';
import './index.css';

class Dashboard extends Component {

    constructor(props) {
      super(props)
      this.state = {
        visitantesRecurrentes: []
      }
    }

    componentDidMount() {
      const email = getToken();
      const apiKey = getApiKey();

      axios.post('http://127.0.0.1:3000/dashboard', {email:email }, {headers: {'apikey': apiKey} })
      .then(response => {
        let visitantesRecurrentes = response.data.visitantesRecurrentes.map(function(vr,i) {
          return (
               <tr key={i}>
                 <td>{vr.ip}</td>
                 <td>{vr.total}</td>
               </tr>
             )

           })
           this.setState(
             {
               visitantesRecurrentes:visitantesRecurrentes
             }
           )

       }).catch(error => {

       });
    }

    render() {
      return (
        <div>
        <table class="table table-striped">
        <thead>
        <tr>
        <td>IPs Recurrentes</td>
        <td>Uso</td>
        </tr>
        </thead>
           <tbody>
           {this.state.visitantesRecurrentes}
           </tbody>
         </table>
        </div>
      );
    }
}


export default Dashboard;
