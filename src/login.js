import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from './Utils/Common';
import './App.css';
function Login(props) {
  const [loading, setLoading] = useState(false);
  const email = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const [errorEmail, setErrorEmail] = useState(null);
  const [errorPass, setErrorPass] = useState(null);

  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setErrorEmail(null);
    setErrorPass(null);

    if(email.value===""){
      setErrorEmail("Cannot be empty");
      return false;
    }else{
      let isValidEmail = validateEmail(email.value);
      if(!isValidEmail){
        setErrorEmail("Invalid Email");
        return false;
      }
    }

    if(password.value===""){
      setErrorPass("Cannot be empty");
      return false;
    }


    setLoading(true);
    
    axios.post('http://127.0.0.1:3000/usuarios/validar_sesion', { email: email.value, password: password.value }).then(response => {
     setLoading(false);
     setUserSession(response.data.nombre, response.data.email,response.data.apikey);
     props.history.push('/UrlShorts');
   }).catch(error => {
     setLoading(false);
     if (error.response.status === 404) setError("Error con el email y/o password");
     else setError("Something went wrong. Please try again later.");
   });
  }

  return (
    <div class="wrapper fadeInDown">
  <div id="formContent">
     <form>
      Login<br /><br />
      <div>
        Email<br />
        <input type="email" {...email} class="fadeIn second" autoComplete="new-password" /><br/>
        <span style={{color: "red"}}>{errorEmail}</span>
      </div>
      <div style={{ marginTop: 10 }}>
        Password<br />
        <input type="password" {...password} class="fadeIn third" autoComplete="new-password" /><br/>
          <span style={{color: "red"}}>{errorPass}</span>
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" class="btn btn-dark" value={loading ? 'Loading...' : 'Ingresar'} onClick={handleLogin} disabled={loading} /><br />
    </form>
    </div>
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}


function validateEmail (email) {
  const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regexp.test(email);
}

export default Login;
