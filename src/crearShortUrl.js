import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from './Utils/Common';
import './App.css';
import { getToken,getApiKey } from './Utils/Common';

function CrearShortUrl(props) {
  const [loading, setLoading] = useState(false);
  const url = useFormInput('');
  const [error, setError] = useState(null);
  const [errorUrl, setErrorUrl] = useState(null);
  const [successMessage,setSuccessMessage] = useState(null);


  const email = getToken();
  const apiKey = getApiKey();

  // handle button click of login form
  const handleRegistro = () => {
    setError(null);
    setErrorUrl(null);
    setLoading(true);
    setSuccessMessage(null);

    if(url.value===""){
      setErrorUrl("Cannot be empty");
      return false;
    }else{
      let isvalidURL = validURL(url.value);
      if(!isvalidURL){
        setErrorUrl("Invalid URL");
        return false;
      }
    }




   axios.post('http://127.0.0.1:3000/create_short_url',{url:url.value,email:email}
   , {headers: {'apikey': apiKey} }
    )
   .then(response => {
     setLoading(false);
     setSuccessMessage("Registro exitoso, en un momento se ira a tu lista de Short Urls");
     var delay = 5000;
     setTimeout(function(){
       props.history.push('/UrlShorts');
     },delay);
   }).catch(error => {
     setLoading(false);
     if (error.response.status === 400) setError(error.response.data.message);
     else setError("Something went wrong. Please try again later.");
   });
  }

  return (
    <div class="wrapper fadeInDown">
  <div id="formContent">
      Registro<br /><br />
      <form>
      <div>
        URL<br />
        <input type="url" {...url} autoComplete="new-password" /><br/>
        <span style={{color: "red"}}>{errorUrl}</span>
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" value={loading ? 'Loading...' : 'Registro'} onClick={handleRegistro} disabled={loading} /><br />
      </form>
      <span style={{color: "green"}}>{successMessage}</span>
      </div>
      </div>
  );
}









  const useFormInput = initialValue => {
    const [value, setValue] = useState(initialValue);

    const handleChange = e => {
      setValue(e.target.value);
    }
    return {
      value,
      onChange: handleChange
    }
  }

  function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return pattern.test(str);
  }

export default CrearShortUrl;
