import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from './Utils/Common';
import './App.css';

function Registro(props) {
  const [loading, setLoading] = useState(false);
  const nombre = useFormInput('');
  const email = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);

  const [successMessage,setSuccessMessage] = useState(null);
  const [errorNombre, setErrorNombre] = useState(null);
  const [errorEmail, setErrorEmail] = useState(null);
  const [errorPass, setErrorPass] = useState(null);



  // handle button click of login form
  const handleRegistro = () => {
    setError(null);
    setSuccessMessage(null);
    setErrorNombre(null);
    setErrorEmail(null);
    setErrorPass(null);


    if(nombre.value===""){
      setErrorNombre("Cannot be empty");
      return false;
    }

    if(email.value===""){
      setErrorEmail("Cannot be empty");
      return false;
    }else{
      let isValidEmail = validateEmail(email.value);
      if(!isValidEmail){
        setErrorEmail("Invalid Email");
        return false;
      }
    }

    if(password.value===""){
      setErrorPass("Cannot be empty");
      return false;
    }

        setLoading(true);
    axios.post('http://127.0.0.1:3000/usuarios/salvar_registro', {nombre:nombre.value, email: email.value, password: password.value }).then(response => {
    setSuccessMessage("Registro exitoso, en un momento se ira a la página de Login");
    var delay = 5000;
    setTimeout(function(){
      props.history.push('/login');
    },delay);

   }).catch(error => {
     setLoading(false);
     if (error.response.status === 401) setError(error.response.data.message);
     else if (error.response.status === 412) setErrorEmail("Email ya se encuentra registrado");
     else setError("Something went wrong. Please try again later.");
   });
  }

  return (
    <div class="wrapper fadeInDown">
  <div id="formContent">
      Registro<br /><br />
      <form>
      <div>
        Nombre<br />
        <input type="nombre" {...nombre} autoComplete="new-password" />
        <br/>
        <span style={{color: "red"}}>{errorNombre}</span>
      </div>
      <div>
        Email<br />
        <input type="email" {...email} autoComplete="new-password" />
        <br/>
        <span style={{color: "red"}}>{errorEmail}</span>
      </div>
      <div style={{ marginTop: 10 }}>
        Password<br />
        <input type="password" {...password} autoComplete="new-password" />
        <br/>
        <span style={{color: "red"}}>{errorPass}</span>
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" value={loading ? 'Loading...' : 'Registro'} onClick={handleRegistro} disabled={loading} /><br />
        <span style={{color: "green"}}>{successMessage}</span>
      </form>
      </div>

      </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

function validateEmail (email) {
  const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regexp.test(email);
}

export default Registro;
